# CSS Grid Layout Zoo

A collection of simple, clearly written example web pages designed to help
learn the new grid layout, with gracious thanks to Rachel Andrew's wonderful
book, [Get Ready for CSS Grid Layout](https://abookapart.com/products/get-ready-for-css-grid-layout). 

* Demo 1: [Defining a Grid](http://htmlpreview.github.io/?https://github.com/novawebdevelopment/gridzoo/blob/master/demo01/index.html)
* Demo 2: [Different Size Boxes](http://htmlpreview.github.io/?https://github.com/novawebdevelopment/gridzoo/blob/master/demo02/index.html)
* Demo 3: [Headers, Footers, etc](http://htmlpreview.github.io/?https://github.com/novawebdevelopment/gridzoo/blob/master/demo03/index.html)
* Demo 3: [Grid Terminology](http://htmlpreview.github.io/?https://github.com/novawebdevelopment/gridzoo/blob/master/demo04/index.html)
